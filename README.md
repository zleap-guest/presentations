# DebianPresentation
A simple presentation on the features of Debian Buster, introductory level (hopefully)

**This project has now been superceeded by https://salsa.debian.org/zleap-guest/bullseyepresentation **

Main Author : Paul Sutton

email: zleap @ disroot . org

irc : zleap

Mailing lists : Users, Publicity & Outreach

[Debian Presentation PDF](https://salsa.debian.org/zleap-guest/presentations/blob/master/DebianPresentation1.pdf)

[Debian Presentation tex (source files)](https://salsa.debian.org/zleap-guest/presentations/blob/master/DebBuster.tex)

**INTRODUCTION**
Document / project is *work in progress*.

I created this as:

 - I was unable to find an updated presentation
 - I wanted something that is ideally beginner level
 - I want to help promote Debian
 - I hope it would be useful to do this
 - Decided on Buster as this is the next release.
 - Based on Default options Gnome 3.x and Wayland. 
 - It should be easy to edit for other configurations
 - It should be easy to update for future versions of Debian or other combinations e.g LXDE, KDE, XFCE and the countless other permutations.

**HOW I WROTE THIS**
LaTeX + Beamer in Overleaf 
References / citations are **not** hyperlinked.
There are some references on individual slides

**LICENSE**

Creative Commons Attribution 4.0 International License.

**ERRORS**

Probably quite a few, presentation is regularly updated and modified.

Xorg version for Debian 10 is unknown as I am unable to find out the version number. 

**CONTRIBUTIONS**

Happy for any help I can get with this and say thank you in advance for any help.  Perhaps send an e-mail with

 - Slide Title
 - Suggested new text
 - Indicate if you would like to be on the contributor list.   (I won't just add you for privacy reasons)

**FINALLY**

Deadline is perhaps when Buster is released, so that people in the community can perhaps use to promote Debian. 

Lets keep it simple.

<!--stackedit_data:
eyJoaXN0b3J5IjpbNTI1MzA4ODY3XX0=
-->
